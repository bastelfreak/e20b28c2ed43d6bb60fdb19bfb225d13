### Keybase proof

I hereby claim:

  * I am bastelfreak on github.
  * I am bastelfreak (https://keybase.io/bastelfreak) on keybase.
  * I have a public key whose fingerprint is C10B 6298 A584 A563 2E25  4DA3 04D6 59E6 BF1C 4CC0

To claim this, I am signing this object:

```json
{
    "body": {
        "key": {
            "eldest_kid": "01017c6c21512988888ff20627d5aa67265ced10cd7c03f2d44709d04de85117d8b70a",
            "fingerprint": "c10b6298a584a5632e254da304d659e6bf1c4cc0",
            "host": "keybase.io",
            "key_id": "04d659e6bf1c4cc0",
            "kid": "01017c6c21512988888ff20627d5aa67265ced10cd7c03f2d44709d04de85117d8b70a",
            "uid": "83c6921276b40940eb86aa1689646b19",
            "username": "bastelfreak"
        },
        "service": {
            "name": "github",
            "username": "bastelfreak"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1488212344,
    "expire_in": 157680000,
    "prev": "4e934ca8554918fa20cae5295d03f0a366cc8bf718b537411ba5afaa79e14601",
    "seqno": 3,
    "tag": "signature"
}
```

with the key [C10B 6298 A584 A563 2E25  4DA3 04D6 59E6 BF1C 4CC0](https://keybase.io/bastelfreak), yielding the signature:

```
-----BEGIN PGP MESSAGE-----

owGtUltIFFEY3q2NzEp8qKQsouli2GJzZs6cmVmEsCihGyZeUortzJkz67S2azuj
JipaCJJUJJRKvXRhoYwi04footVGL6GVohVdHixLrSiEiqTbmcqnoKf+l8P5zvd9
fOfjPzxzsivO7RkoeNU1f+MN990RzbW1Y0tNJaeF9QrOV8kF6a+DFuvUsv1BU+d8
HA94IBNEBCABQVWcMQyBR4KsSxgjWUASoTrgiS4TXjQEHUKZV3Ue6lSRAJB1RZN5
zHk5wwwFaKQkYoZsZksAryHmhyUFYgmJAhUkqGOR6ZCkUqQZgEBCeCYsCluOgoXT
sEXTzDDD2MX/O97f/P+cu/SXnSISpApAkJEGeRXyVFMQxgApKoJIA6pDtGgkhHdR
xmZBbVpsRCgOctVejj2UmYQ63f4hBEy7qFT7p8iuKHHQcqr5/+j9mhnSWYtMVkYj
lhkOcT7AmMQ2HQMAFYUlFCH0cnRPiRmhftNhSDJSeDZeriRCy5glpKoICVakPAmq
QDGwwBNMJUGVdNYEj0WECFE0QwaKJokyBEDDEjYwllUKIOIB53xpdyjM+USWEweY
p2UGQtgujVCuOn6/W/S43HGuBXMWeW5O11qidVGStnTT6YnNmzLJWTtX/LTECeTe
+ak/Vqe1t9eGP36ALWvkFVa6XP6wvWf5yIvnLV9iBf6u2JIL2W2e6J3s/EOJB3tm
+2oOHYhudjfZObn22eZ50c/Xe5Nr1f69GVUv++duq754Kmud/ejJYNIgSbbGVnnb
vu0cX3vsTFIsvnE73NPc9qx+3tdPSvfrJ7nfjpzofLfpuNCDpm99mvfmQexy58LM
/PHEYEPh1aHC0abUjA/F1olTy7p9Oy4lvM1yN7Wu3FlZ1PrVs+399/lJn+oaR5uy
ShNix7uf3R1efK7vzOP+8isne+WM+qF9RZk5+rqxlOH1J7dsGMicUXX7/qrNxq26
/P4+btCbMPfKtfSG3oxZnSltR5d1NKce/Ak=
=7rI6
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/bastelfreak

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id bastelfreak
```
